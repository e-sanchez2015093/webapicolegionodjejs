var database = require("../config/database.config");
var SeccionAlumno = {};

SeccionAlumno.select = function(idUsuario,callback) {
  if(database) {
		database.query('CALL sp_seccionAlumnoMateria(?)',idUsuario,
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados[0]);
			}
		});
	}
}
SeccionAlumno.selectEs = function(idAlumno,callback) {
  if(database) {
		database.query('CALL sp_selectAlumno()',
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados[0]);
			}
		});
	}
}

SeccionAlumno.insert = function(data, callback) {
  if(database) {
    database.query('CALL sp_insertarSeccionMateria(?,?,?)',
    [data.idUsuario,data.token,data.idSeccion],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
      	console.log(resultado);
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

SeccionAlumno.update = function(data, callback){
	if(database) {
        //console.log("Antes de enviar a la base de datos");
        //console.log(data);
		database.query('CALL sp_updateAlumno(?,?,?,?,?,?,?)',
		[data.idAlumno, data.idUsuario, data.nombre, data.apellido, data.imagen, data.nick,data.contrasena],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado);
			}
		});
	}
}

SeccionAlumno.delete = function(idAlumno, callback) {
	if(database) {
        console.log(idAlumno);
		database.query('CALL sp_deleteAlumno(?)', idAlumno,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = SeccionAlumno;
