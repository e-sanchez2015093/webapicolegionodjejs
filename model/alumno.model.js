var database = require("../config/database.config");
var Alumno = {};

Alumno.select = function(callback) {
  if(database) {
		database.query('CALL sp_selectAlumno()',
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados[0]);
			}
		});
	}
}
Alumno.selectEs = function(idAlumno,callback) {
  if(database) {
		database.query('CALL sp_selectAlumno()',
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados[0]);
			}
		});
	}
}

Alumno.insert = function(data, callback) {
  if(database) {
    database.query('CALL sp_insertContacto(?,?,?,?,?,?)',
    [data.idUsuario, data.nombre, data.apellido, data.telefono, data.direccion, data.idCategoria],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
      	console.log(resultado);
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

Alumno.update = function(data, callback){
	if(database) {
        //console.log("Antes de enviar a la base de datos");
        //console.log(data);
		database.query('CALL sp_updateAlumno(?,?,?,?,?,?,?)',
		[data.idAlumno, data.idUsuario, data.nombre, data.apellido, data.imagen, data.nick,data.contrasena],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado);
			}
		});
	}
}

Alumno.delete = function(idAlumno, callback) {
	if(database) {
        console.log(idAlumno);
		database.query('CALL sp_deleteAlumno(?)', idAlumno,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = Alumno;
