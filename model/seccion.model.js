var database = require("../config/database.config");
var Seccion = {};

Seccion.select = function(callback) {
  if(database) {
		database.query('CALL sp_selecSeccion()',
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados[0]);
			}
		});
	}
}
Seccion.selectEspe = function(idGrado,callback) {
  if(database) {
      console.log(idGrado)
		database.query('CALL sp_selecSeccionEspe(?)',idGrado,
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados[0]);
			}
		});
	}
}

Seccion.insert = function(data, callback) {
  if(database) {
    database.query('CALL sp_insertContacto(?,?,?,?,?,?)',
    [data.idUsuario, data.nombre, data.apellido, data.telefono, data.direccion, data.idCategoria],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
      	console.log(resultado);
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

Seccion.update = function(data, callback){
	if(database) {
        //console.log("Antes de enviar a la base de datos");
        //console.log(data);
		database.query('CALL sp_updateAlumno(?,?,?,?,?,?,?)',
		[data.idAlumno, data.idUsuario, data.nombre, data.apellido, data.imagen, data.nick,data.contrasena],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado);
			}
		});
	}
}

Seccion.delete = function(idAlumno, callback) {
	if(database) {
        console.log(idAlumno);
		database.query('CALL sp_deleteAlumno(?)', idAlumno,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = Seccion;
