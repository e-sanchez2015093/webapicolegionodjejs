var database = require("../config/database.config");
var actividadSeccion = {};

actividadSeccion.select = function(idUsuario,callback) {
  if(database) {
		database.query('CALL sp_selectActividad(?)',idUsuario,
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados[0]);
			}
		});
	}
}
actividadSeccion.selectEs = function(idAlumno,callback) {
  if(database) {
		database.query('CALL sp_selectAlumno()',
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados[0]);
			}
		});
	}
}

actividadSeccion.insert = function(data, callback) {
  if(database) {
		console.log("Esta en el insertar");
		console.log("Esta en el insertar");
		console.log(data);
/*
 in _idMateria int,
    in _descripcion varchar(50),
    in _fechafin datetime,
    in _bimestre int,
    in _valor int,
    in _idUsuario int,
    in _idSeccion int

*/

    database.query('CALL sp_insertarActividad(?,?,?,?,?,?,?)',
    [data.idMateria,data.descripcion,data.bimestre,data.valor,data.fecha_final,data.idUsuario,data.idSeccion],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
      	console.log(resultado);
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

actividadSeccion.update = function(data, callback){
	if(database) {
        //console.log("Antes de enviar a la base de datos");
        //console.log(data);
		database.query('CALL sp_updateAlumno(?,?,?,?,?,?,?)',
		[data.idAlumno, data.idUsuario, data.nombre, data.apellido, data.imagen, data.nick,data.contrasena],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado);
			}
		});
	}
}

actividadSeccion.delete = function(idAlumno, callback) {
	if(database) {
        console.log(idAlumno);
		database.query('CALL sp_deleteAlumno(?)', idAlumno,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = actividadSeccion;
