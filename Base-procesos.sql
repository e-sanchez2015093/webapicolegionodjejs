CREATE DATABASE Colegio2;
Use Colegio2;
CREATE TABLE TipoUsuario(
	idTipoUsuario INT NOT NULL AUTO_INCREMENT,
    nombre varchar(100) not null,
    primary key(idTipoUsuario)
);
CREATE TABLE Usuario(
	idUsuario INT NOT NULL AUTO_INCREMENT,
    nick varchar(100) not null,
    contrasena varchar(100) not null,
    idTipoUsuario int not null,
    primary key(idUsuario),
	FOREIGN KEY(idTipoUsuario) REFERENCES TipoUsuario(idTipoUsuario)
    
);
CREATE TABLE Alumno(
	idAlumno INT NOT NULL AUTO_INCREMENT,
    nombre varchar(100) not null,
    apellido varchar(100) not null,
    idUsuario int not null,
    imagen varchar(100),
    primary key(idAlumno),
    FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario)
);
CREATE TABLE Profesor(
	idProfesor INT NOT NULL AUTO_INCREMENT,
    nombre varchar(50) not null,
    apellido varchar(50) not null,
    direccion varchar(100) not null,
    telefono varchar(20) not null,
    idUsuario int not null,
    primary key(idProfesor),
    foreign key(idUsuario) REFERENCES Usuario(idUsuario)

);
CREATE TABLE Grado(
	idGrado INT NOT NULL AUTO_INCREMENT,
    nombre varchar(50),
    primary key(idGrado)
);
CREATE TABLE Jornada(
	idJornada INT NOT NULL AUTO_INCREMENT,
    nombre varchar(50),
    primary key(idJornada)
);
CREATE TABLE Seccion(
	idSeccion INT NOT NULL AUTO_INCREMENT,
    nombre varchar(50),
    idGrado int not null,
    idJornada int not null,
    primary key(idSeccion),
    foreign key(idGrado) REFERENCES Grado(idGrado),
    foreign key(idJornada) REFERENCES Jornada(idJornada)
    
);
CREATE TABLE Token(
	idToken INT NOT NULL AUTO_INCREMENT,
    token varchar(200) not null,
    primary key(idToken)
    
);

CREATE TABLE Materia(
idMateria INT NOT NULL AUTO_INCREMENT,
    nombre varchar(50),
    idToken int,
    primary key(idMateria),
    FOREIGN key(idToken) references token(idToken)
);
CREATE TABLE SeccionAlumnoMateria(
	idSeccionMA INT NOT NULL AUTO_INCREMENT,
    idAlumno int not null,
    idMateria int not null,
    idSeccion int null,
    primary key(idSeccionMA),
    FOREIGN key(idAlumno) references Alumno(idAlumno),
    FOREIGN key(idMateria) references Materia(idMateria),
    FOREIGN key(idSeccion) references Seccion(idSeccion)
);
CREATE TABLE MateriaProfesor(
	idMateriaProfesor INT NOT NULL AUTO_INCREMENT,
    idMateria INT NOT NULL,
    idProfesor INT NOT NULL,
    PRIMARY KEY(idMateriaProfesor),
    FOREIGN KEY(idMateria) references materia(idMateria),
    FOREIGN KEY(idProfesor) references profesor(idProfesor)
);

CREATE TABLE MateriaProfesorSeccion(
	idMateriaProfesorSeccion INT NOT NULL AUTO_INCREMENT,
    idProfesor INT NOT NULL,
    idSeccion int not null,
    PRIMARY KEY(idMateriaProfesorSeccion),
    FOREIGN KEY(idProfesor) references profesor(idProfesor),
    FOREIGN KEY(idSeccion) references seccion(idSeccion)
);
CREATE TABLE Actividad(
	idActividad INT NOT NULL AUTO_INCREMENT,
    idMateria INT NOT NULL,
    descripcion varchar(100) not null,
    fechaInicio datetime,
    fechaFin datetime,
    idBimestre int not null,
    valor int not null,
    primary key(idActividad),
    FOREIGN KEY(idMateria) references Materia(idMateria)
    
);
CREATE TABLE DetalleActividad(
	idDetalleActividad INT NOT NULL AUTO_INCREMENT,
    idActividad INT NOT NULL,
    idSeccion int not null,
    idProfesor int not null,
    primary key (idDetalleActividad),
    FOREIGN KEY(idActividad) references Actividad(idActividad),
    FOREIGN KEY(idSeccion) references Seccion(idSeccion),
    FOREIGN KEY(idProfesor) references Profesor(idProfesor)
    
    
);

CREATE TABLE ActividadAlumno(
	idActividadAlumno INT NOT NULL AUTO_INCREMENT,
    idAlumno INT NOT NULL ,
    idActividad INT NOT NULL,
    archivo VARCHAR(100) null,
    PRIMARY KEY(idActividadAlumno),
    FOREIGN KEY(idAlumno) REFERENCES Alumno (idAlumno),
    FOREIGN KEY(idActividad) REFERENCES Actividad(idActividad)
);

select * from tipoUsuario;

DELIMITER $$
CREATE PROCEDURE sp_insertarActividad(

    in _idMateria int,
    in _descripcion varchar(50),
    in _bimestre int,
    in _valor int,
    in _fechaFin datetime,
    in _idUsuario int,
    in _idSeccion int
    
    
)
begin 
	declare _idMateria2 int default 0;
    declare _idSeccion2 int default 0;
    declare _idProfesor int default 0;
    declare _idAlumno int default 0;
	set _idAlumno = (SELECT idAlumno from SeccionAlumnoMateria where idMateria = _idMateria);
    set _idMateria2 = (SELECT idMateria from SeccionAlumnoMateria where idMateria = _idMateria);
    set _idSeccion2 = (SELECT count(idSeccion) from SeccionAlumnoMateria where idSeccion = _idSeccion);
    set _idProfesor = (select idProfesor from Profesor where idUsuario = _idUsuario);
    if _idMateria2 <> 0 then
		if _idSeccion2 != 0 then
			insert into Actividad(idMateria,descripcion,fechaInicio,fechaFin,idBimestre,valor) 
            values(_idMateria,_descripcion,NOW(),_fechaFin,_bimestre,_valor);
            insert into DetalleActividad(idActividad,idSeccion,idProfesor)
            values((SELECT MAX(idActividad) as id from Actividad),_idSeccion,_idProfesor);
            if _idAlumno != 0 then
            insert into ActividadAlumno(idAlumno,idActividad) values (_idAlumno,(SELECT MAX(idActividad) as id from Actividad));
            end if;
		end if;
	end if;

END$$

DELIMITER ;



DELIMITER $$
CREATE PROCEDURE sp_insertarSeccionMateria(
    in _idUsuario int,
    in _Token varchar(50),
    in _idSeccion int 
)
begin 
	declare _idAlumno int default 0;
	declare _idMateria int default 0;
	declare _idToken int default 0;
    set _idAlumno = (Select idAlumno from Alumno where idUsuario = _idUsuario);
    set _idToken = (select  idToken from Token where token = _Token);
    set _idMateria = (Select idMateria from Materia where idToken = _idToken);
	insert into SeccionAlumnoMateria(idAlumno,idMateria,idSeccion) values (_idAlumno,_idMateria,_idSeccion);

END$$

DELIMITER ;

Select * from Token;


DELIMITER $$
CREATE PROCEDURE sp_autenticarUsuario(

    in _nick varchar(100),
    in _contrasena varchar(100)
    
)
begin 
	select * from Usuario where nick = _nick and contrasena = _contrasena;


END$$

DELIMITER ;

DELIMITER $$
CREATE PROCEDURE sp_insertarAlumno
(
	in _Nombre varchar(50),
	in _Apellido varchar(50),
	in _Imagen varchar(100),
    in _nick varchar(50),
    in _contrasena varchar(50)
)
BEGIN
insert into Usuario (nick,contrasena,idTipoUsuario) values (_nick,_contrasena,2);
insert into Alumno(nombre,apellido,idUsuario,imagen) values(_Nombre,_Apellido,(Select Max(idUsuario) as id from Usuario),_Imagen);
END$$

DELIMITER ;
DELIMITER $$
CREATE PROCEDURE sp_updateAlumno
(
	in _IdAlumno int,
    in _IdUsuario int,
	in _Nombre varchar(50),
	in _Apellido varchar(50),
	in _Imagen varchar(100),
    in _nick varchar(50),
    in _contrasena varchar(50)
)
BEGIN
update Usuario set nick = _nick, contrasena = _contrasena where idUsuario = _IdUsuario;
update Alumno set nombre = _Nombre, apellido = _Apellido, imagen = _Imagen where idAlumno = _IdAlumno;
END$$

DELIMITER ;

DELIMITER $$
CREATE PROCEDURE sp_deleteAlumno
(
	in _IdAlumno int
)
BEGIN
declare _idUsuario int default 0;
 set _idUsuario = (select idUsuario from Alumno where idAlumno = _IdAlumno);
 delete from Alumno where idAlumno = _IdAlumno;
 delete from Usuario where idUsuario = _idUsuario;
END$$

DELIMITER ;





DELIMITER $$
CREATE PROCEDURE sp_insertarProfesor
(
	in _Nombre varchar(50),
	in _Apellido varchar(50),
	in _Direccion varchar(50),
    in _Telefono varchar(50),
    in _nick varchar(50),
    in _contrasena varchar(50)
)
BEGIN
insert into Usuario (nick,contrasena,idTipoUsuario) values (_nick,_contrasena,3);
insert into Profesor(nombre,apellido,direccion,telefono,idUsuario) values(_Nombre,_Apellido,_Direccion,_Telefono,(Select Max(idUsuario) as id from Usuario));
END$$

DELIMITER ;

DELIMITER $$
CREATE PROCEDURE sp_selectAlumno()
BEGIN
select * from vista_alumno;
END$$

DELIMITER ;
call sp_selectAlumno;
CREATE view 
vista_alumno
as 
select a.idAlumno,a.nombre,a.apellido,a.idUsuario, a.imagen,user.nick,user.contrasena
from Alumno as a
inner join Usuario as user on a.idUsuario = user.idUsuario order by a.apellido asc;


call sp_autenticarUsuario('Elmer','123');
select * from Alumno;
delete from Usuario where idUsuario = 7;
insert into Usuario(nick,contrasena,idTipoUsuario) values ('Elmer','123',1);
insert into Usuario(nick,contrasena,idTipoUsuario) values ('Gustavo','123',2);
call sp_deleteAlumno(5)



DELIMITER $$
CREATE PROCEDURE sp_insertarProfesor
(
in _Nombre varchar(50),
in _Apellido varchar(50),
in _Direccion varchar(50),
    in _Telefono varchar(50),
    in _nick varchar(50),
    in _contrasena varchar(50)
)
BEGIN
insert into Usuario (nick,contrasena,idTipoUsuario) values (_nick,_contrasena,3);
insert into Profesor(nombre,apellido,direccion,telefono,idUsuario) values(_Nombre,_Apellido,_Direccion,_Telefono,(Select Max(idUsuario) as id from Usuario));
END$$

DELIMITER ;


DELIMITER $$
create PROCEDURE sp_updateProfesor
(
in _Nombre varchar(50),
in _Apellido varchar(50),
in _Direccion varchar(50),
    in _Telefono varchar(50),
in _idProfesor int
)
BEGIN
update profesor set nombre = _Nombre, apellido = _Apellido, direccion = _Direccion,
telefono = _Telefono where idProfesor = _idProfesor;

END$$

DELIMITER ;


DELIMITER $$
CREATE PROCEDURE sp_deleteProfesor
(
in _idProfesor int
)
BEGIN
    delete  from profesor where idProfesor = _idProfesor;
END$$

DELIMITER ;





DELIMITER $$
CREATE PROCEDURE spinsertarMateria
(
in _nombre varchar(50),
    in _token Varchar(50) 
)
BEGIN
insert into token(token) values(_token);
    insert into materia(nombre, idToken) values(_nombre, (SELECT MAX(idToken) as id from token));

END$$

DELIMITER ;


DELIMITER $$
CREATE PROCEDURE spModificarMateria
(
in _nombre varchar(50),
    in _id int
)
BEGIN

update materia set nombre = _nombre where idMateria = _id;
END$$

DELIMITER ;



DELIMITER $$
CREATE PROCEDURE spEliminarMateria
(
in _idMateria int
)
BEGIN
    delete from materia where idMateria = _idMateria;
    
END$$

DELIMITER ;

	DELIMITER $$
create PROCEDURE sp_Materia()
BEGIN
	SELECT m.idMateria as idMateria, m.nombre as nombre, t.token as token from materia as m inner join token as t on m.idToken = t.idToken;

END$$
DELIMITER ;
insert into Token(token) values ('skdjflkds');
call spinsertarMateria('Sociales',23434234);

select * from Token;



DELIMITER $$
create PROCEDURE sp_insertarAsignacion
(
	in _idMateria int,
    in _idProfesor int,
    in _idSeccion int
)
BEGIN
DECLARE idRepetido INT;  
  
    set idRepetido = (SELECT count(idProfesor) from MateriaProfesor where idProfesor = _idProfesor);
	IF idRepetido != 1 THEN 
		insert into materiaProfesor(idMateria, idProfesor) values(_idMateria, _idProfesor);
	
    END IF;
    
    if idRepetido = 1 then 
			insert into MateriaProfesorSeccion( idProfesor, idSeccion) values(_idProfesor, _idSeccion);

    end if;
    

END$$
DELIMITER ;


insert into Grado(nombre) values ('Cuarto');
insert into Grado(nombre) values ('Quinto');
insert into Grado(nombre) values ('Sexto');

insert into Jornada(nombre) values ('Matutina');
insert into Jornada(nombre) values ('Vespertina');

insert into Seccion(nombre,idGrado,idJornada) values ('A',1,2);
insert into Seccion(nombre,idGrado,idJornada) values ('B',1,2);
insert into Seccion(nombre,idGrado,idJornada) values ('C',2,1);

create view 
vie_seccion
as
select s.idSeccion,s.nombre as Seccion, s.idGrado,gr.nombre as Grado,s.idJornada,jor.nombre as Jornada 
from Seccion as s
inner join Grado as gr on s.idGrado = gr.idGrado
inner join Jornada as jor on s.idJornada = jor.idJornada order by Grado;

CREATE view 
vista_SeccionAlumno
as 
select sma.idSeccionMA,sma.idSeccion, sec.nombre, sma.idMateria, mat.nombre as Materia,tok.token, jor.nombre as Jornada,gr.nombre as Grado,sma.idAlumno,al.idUsuario, al.Nombre As Alumno 
from SeccionAlumnoMateria as sma
inner join Seccion as sec on sma.idSeccion = sec.idSeccion
inner join Materia as mat on sma.idMateria = mat.idMateria
inner join Alumno as al on sma.idAlumno = al.idAlumno
inner join Jornada as jor on sec.idJornada = jor.idJornada
inner join Grado as gr on sec.idGrado = gr.idGrado
inner join Token as tok on mat.idToken = tok.idToken;


select * from vista_SeccionAlumno where idUsuario = 1;
DELIMITER $$
CREATE PROCEDURE sp_seccionAlumnoMateria(
    in _idUsuario int 
)
begin 
select * from vista_SeccionAlumno where idUsuario = _idUsuario;
END$$

DELIMITER ;


DELIMITER $$
CREATE PROCEDURE sp_selecSeccionEspe(
    in _idGrado int 
)
begin 
select * from vie_seccion where idGrado = _idGrado;
END$$

DELIMITER ;

DELIMITER $$
CREATE PROCEDURE sp_selecSeccion(
    
)
begin 
select * from vie_seccion ;
END$$

DELIMITER ;

call sp_selecSeccionEspe(1);

select * from TipoUsuario;
select * from Alumno ;
select * from Materia ;
select * from Seccion;
insert into SeccionAlumnoMateria(idAlumno,idMateria,idSeccion) values (4,1,1);
call sp_seccionAlumnoMateria(11);

select * from Usuario;
select * from DetalleActividad;
select * from Seccion;

insert into Usuario(nick,contrasena,idTipoUsuario) values('PGustavo','123',3);
insert into Profesor(nombre,apellido,direccion,telefono,idUsuario) values ('Gustavin','Sanchez','zon 11','3243423',15);


call sp_insertarActividad(6,'entrega de hoja 4',1,20,15,10);


create view 
vista_Actividad
as
select act.idActividad, act.idMateria, mat.nombre as Materia, act.descripcion,
date_format(fechaInicio, '%d-%c-%Y') as 'fecha_registro',
date_format(fechaInicio, '%h:%i:%s') as 'hora_registro',
date_format(fechaFin , '%d-%c-%Y') as 'fecha_entrega',
date_format(fechaFin ,'%h:%i:%s') as 'hora_entrega',
act.idBimestre as Bimestre, act.valor, sec.idSeccion, sec.nombre as Seccion, prof.idProfesor, prof.idUsuario,prof.nombre as Profesor
from Actividad act
inner join Materia as mat on act.idMateria = mat.idMateria
inner join DetalleActividad as det  on det.idActividad = act.idActividad
inner join Profesor as prof on det.idProfesor = prof.idProfesor
inner join Seccion as sec on det.idSeccion = sec.idSeccion; 


DELIMITER $$
CREATE PROCEDURE sp_selectActividad(
    in _idUsuario int
)
begin 
select * from vista_Actividad where idUsuario =_idUsuario ;
END$$

DELIMITER ;
call sp_selectActividad(15);
select * from ActividadAlumno;
select * from Alumno;
call sp_selectActividad(11);

CREATE view
actividad_Alumno
as
select ac.idActividadAlumno,ac.idActividad,ac.idAlumno,al.nombre as Alumno,al.idUsuario,ac.archivo,acSec.descripcion as Actividad,
date_format(acSec.fechaInicio, '%d-%c-%Y') as 'Fecha_Asignacion', date_format(acSec.fechaFin,'%d-%c-%Y') as 'Fecha_Entrega',
mat.nombre as Materia
from ActividadAlumno ac
inner join Actividad as acSec on ac.idActividad = acSec.idActividad
inner join Alumno as al on ac.idAlumno = al.idAlumno
inner join Materia as mat on acSec.idMateria = mat.idMateria order by acSec.fechaFin;

DELIMITER $$
CREATE PROCEDURE sp_selectActividadAlumno(
    in _idUsuario int
)
begin 
select * from actividad_Alumno where idUsuario =_idUsuario ;
END$$

DELIMITER ;
Select * from TipoUsuario;
call sp_selectActividadAlumno(11);

select * from ActividadAlumno;


DELIMITER $$
CREATE PROCEDURE sp_updatActvidadAlumno(
    in _idActividadAlumno int,
    in _archivo varchar(100)
)
begin 
update ActividadAlumno set archivo = _archivo where idActividadAlumno = _idActividadAlumno;
END$$

DELIMITER ;
