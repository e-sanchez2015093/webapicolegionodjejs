var express = require('express');
var actividadSeccion = require('../../model/actividadSeccion.model');
var services = require('../../services');
var router = express.Router();

router.get('/actividadseccion/', services.verificar, function(req, res, next) {
  var idUsuario = req.usuario.idUsuario;
  actividadSeccion.select(idUsuario, function(seccionAlumno) {
    if(typeof seccionAlumno !== 'undefined') {
      res.json(seccionAlumno);
    } else {
      res.json({"mensaje" : "No hay contactos"});
    }
  });
});
//localhost:3000/api/v1/contacto/4

router.get('/actividadseccion/:idActividad', services.verificar, function(req, res, next) {
console.log("get");
console.log("get");    
console.log("Meto get de en especifico");

  var idActividad = req.params.idActividad;
  var idUsuario = req.usuario.idUsuario;
  console.log(idActividad);
  actividadSeccion.select(idUsuario, function(seccion) {
    if(typeof seccion !== 'undefined') {
     console.log("Adentro del if");
      res.json(seccion.find(c => c.idActividad == idActividad));
      //console.log(seccion.find(c => c.idActividad == idActividad));
      //console.log(res.json(seccion.find(c => c.idSeccionMA == idSeccionMA)));
    } else {
      res.json({"mensaje" : "No hay contactos"});
    }
  });
});

router.post('/actividadseccion', services.verificar, function(req, res, next) {
console.log("Esta en le post");
console.log("metod Post");
console.log("metod Post");
console.log("metod Post");
  //console.log(req.usuario);
  var data = {
    idMateria : req.body.idMateria,
    descripcion : req.body.descripcion,
    bimestre : req.body.bimestre,
    valor:req.body.valor,
    fecha_final:req.body.fecha_final,
    idUsuario: req.usuario.idUsuario,
    
    
    //fecha_entrega: req.body.fecha_entrega,
    
    
    idSeccion:req.body.idSeccion
  };
  console.log(data);
  actividadSeccion.insert(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se agrego el contacto"
      });
    } else {
      res.json({"mensaje":"No se ingreso el contacto"});
    }
  });
});

router.put('/contacto/:idContacto', function(req, res, next){
  var idContacto = req.params.idContacto;
  var data = {
    nombre : req.body.nombre,
    apellido : req.body.apellido,
    telefono : req.body.telefono,
    direccion : req.body.direccion,
    idCategoria : req.body.idCategoria,
    idContacto : idContacto
  }
  seccionAlumno.update(data, function(resultado){
    if(resultado.length > 0) {
      res.json({
        estado: true,
        mensaje: "Se ha modificado con exito"
      });
    } else {
      res.json({
        estado: false,
        mensaje: "No se pudo modificar"
      });
    }
  });
});

router.delete('/contacto/:idContacto', function(req, res, next){
  var idContactoUri = req.params.idContacto;
  actividadSeccion.delete(idContactoUri, function(resultado){
    if(resultado && resultado.mensaje ===	"Eliminado") {
      res.json({
        estado: true,
        "mensaje":"Se elimino el contacto correctamente"
      });
    } else {
      res.json({
        estado: false,
        "mensaje":"No se elimino el contacto"});
    }
  });
});

module.exports = router;
