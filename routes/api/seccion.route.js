var express = require('express');
var seccion = require('../../model/seccion.model');
var services = require('../../services');
var router = express.Router();

router.get('/seccion/', services.verificar, function(req, res, next) {
  var idUsuario = req.usuario.idUsuario;
  seccion.select( function(alumno) {
    if(typeof alumno !== 'undefined') {
      res.json(alumno);
    } else {
      res.json({"mensaje" : "No hay contactos"});
    }
  });
});
//localhost:3000/api/v1/contacto/4

router.get('/seccion/:id', services.verificar, function(req, res, next) {
  var idGrado = req.params.id;
  var alumnos;
  console.log(idGrado)
 // var idUsuario = req.usuario.idUsuario;
  seccion.selectEspe( idGrado,function(seccion) {
    if(typeof seccion !== 'undefined') {
      res.json(seccion);
      //alumnos = alumno;
      //console.log(alumnos);
    } else {
      res.json({"mensaje" : "No hay secciones"});
    }
  });
});

router.post('/contacto', services.verificar, function(req, res, next) {
  console.log(req.usuario);
  
  var data = {
    idUsuario: req.usuario.idUsuario,
    nombre : req.body.nombre,
    apellido : req.body.apellido,
    telefono : req.body.telefono,
    direccion : req.body.direccion,
    idCategoria : req.body.idCategoria
  };
  console.log(data);
  alumno.insert(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se agrego el contacto"
      });
    } else {
      res.json({"mensaje":"No se ingreso el contacto"});
    }
  });
});

router.put('/alumno/:idAlumno', function(req, res, next){
  var idAlumno = req.params.idAlumno;
  var foto ="Nulo";
  var data = {
    nombre : req.body.nombre,
    apellido : req.body.apellido,
    nick : req.body.nick,
    contrasena : req.body.contrasena,
    idAlumno : req.body.idAlumno,
    idUsuario : req.body.idUsuario,
    imagen:foto
  }
//console.log(data);
  alumno.update(data, function(resultado){
    if(resultado.length > 0) {
      res.json({
        estado: true,
        mensaje: "Se ha modificado con exito"
      });
    } else {
      res.json({
        estado: false,
        mensaje: "No se pudo modificar"
      });
    }
  });
});

router.delete('/alumno/:idAlumno', function(req, res, next){
  var idAlumnoUri = req.params.idAlumno;
  alumno.delete(idAlumnoUri, function(resultado){
    if(resultado && resultado.mensaje ===	"Eliminado") {
      res.json({
        estado: true,
        "mensaje":"Se elimino el alumno correctamente"
      });
    } else {
      res.json({
        estado: false,
        "mensaje":"No se elimino el contacto"});
    }
  });
});

module.exports = router;